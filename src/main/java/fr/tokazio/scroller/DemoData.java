package fr.tokazio.scroller;

import java.util.Random;

public class DemoData {

    private String colA;
    private String colB;
    private String colC;
    private String colD;
    private String colE;
    private String colF;
    private String colG;

    public DemoData() {
        colA = randomString();
        colB = randomString();
        colC = randomString();
        colD = randomString();
        colE = randomString();
        colF = randomString();
        colG = randomString();
    }

    private String randomString() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

}

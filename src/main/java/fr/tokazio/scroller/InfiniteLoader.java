package fr.tokazio.scroller;

import java.util.List;

public interface InfiniteLoader<DATA> {

    List<DATA> load(int from, InfiniteWorker worker);

}

package fr.tokazio.scroller;

import javax.swing.table.TableModel;
import java.util.List;

public interface InfiniteModel<DATA> extends TableModel {

    <T extends InfiniteModel<DATA>> T addDatas(final List<DATA> dataList);
}

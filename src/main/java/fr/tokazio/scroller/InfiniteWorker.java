package fr.tokazio.scroller;

public interface InfiniteWorker {
    void progress(int value);

    void max(int maxValue);

    void min(int minValue);
}

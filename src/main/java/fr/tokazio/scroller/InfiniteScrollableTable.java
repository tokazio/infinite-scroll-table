package fr.tokazio.scroller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.List;
import java.util.logging.Logger;

public class InfiniteScrollableTable<DATA> {

    private static final Logger LOGGER = Logger.getLogger(InfiniteScrollableTable.class.getName());

    private final InfiniteModel<DATA> model;

    private final JPanel panel;
    private final JPanel barPanel;
    private final JProgressBar bar;
    private final JTable table;
    private final JScrollPane pane;
    private float percentScrollWhenLoad = 50;
    private volatile boolean loading = false;
    private InfiniteLoader<DATA> loader;
    private boolean hasMore = true;

    public InfiniteScrollableTable(final InfiniteModel<DATA> model) {
        this.model = model;
        this.panel = new JPanel();
        this.panel.setOpaque(true);
        this.panel.setBackground(Color.WHITE);
        this.panel.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createEmptyBorder(5, 5, 5, 5),
                        BorderFactory.createLineBorder(Color.GRAY)
                ));
        this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.Y_AXIS));
        this.table = new JTable(model);
        this.table.setBorder(BorderFactory.createEmptyBorder());
        this.pane = new JScrollPane(table);
        this.pane.setBorder(BorderFactory.createEmptyBorder());

        this.barPanel = new JPanel();
        this.barPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
        this.barPanel.setLayout(new GridLayout(2, 1));
        this.barPanel.setBackground(new Color(255, 238, 128));
        JLabel lbl = new JLabel("Chargement de la suite...");
        lbl.setHorizontalAlignment(SwingConstants.CENTER);
        this.barPanel.add(lbl);
        bar = new JProgressBar();
        this.barPanel.add(bar);
        bar.setIndeterminate(true);

        panel.add(pane);
        panel.add(barPanel);

        this.pane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                float maxValue = pane.getVerticalScrollBar().getMaximum() - pane.getVerticalScrollBar().getVisibleAmount();
                int currentValue = pane.getVerticalScrollBar().getValue();
                int fraction = (int) ((currentValue / maxValue) * 100);
                if (fraction >= percentScrollWhenLoad) {
                    load();
                }
            }
        });
    }


    public InfiniteScrollableTable<DATA> setLoader(final InfiniteLoader<DATA> loader) {
        this.loader = loader;
        load();
        return this;
    }

    private void load() {
        if (!loading && hasMore && loader != null) {
            loading = true;
            LOGGER.info("Load more!");
            this.barPanel.setVisible(true);
            new Worker().execute();
        }
    }

    public Component asComponent() {
        return panel;
    }

    private class Worker extends SwingWorker<List<DATA>, Integer> implements InfiniteWorker {

        @Override
        protected List<DATA> doInBackground() {
            final List<DATA> newDatas = loader.load(model.getRowCount(), this);
            if (newDatas.isEmpty()) {
                hasMore = false;
            }
            //TODO get row position to reset it after the load
            model.addDatas(newDatas);
            return newDatas;
        }

        protected void process(List<Integer> chunks) {
            bar.setValue(chunks.get(0));
        }

        protected void done() {
            loading = false;
            barPanel.setVisible(false);
        }

        @Override
        public void progress(int value) {
            publish(value);
        }

        @Override
        public void max(int maxValue) {
            bar.setIndeterminate(maxValue <= 0);
            bar.setMaximum(maxValue);
        }

        @Override
        public void min(int minValue) {
            bar.setMinimum(minValue);
        }
    }

}

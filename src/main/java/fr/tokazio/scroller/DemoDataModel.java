package fr.tokazio.scroller;

import javax.swing.table.AbstractTableModel;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

public class DemoDataModel extends AbstractTableModel implements InfiniteModel<DemoData> {

    private final List<DemoData> demoData = new LinkedList<>();

    public DemoDataModel() {
        super();
    }

    @Override
    public <T extends InfiniteModel<DemoData>> T addDatas(final List<DemoData> demoDatas) {
        if (demoDatas != null) {
            this.demoData.addAll(demoDatas);
            fireTableDataChanged();
        }
        return (T) this;
    }

    @Override
    public int getRowCount() {
        return demoData.size();
    }

    @Override
    public int getColumnCount() {
        return DemoData.class.getDeclaredFields().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        final DemoData row = demoData.get(rowIndex);
        try {
            final Field f = DemoData.class.getDeclaredFields()[columnIndex];
            f.setAccessible(true);
            return f.get(row);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String getColumnName(int columnIndex) {
        final Field f = DemoData.class.getDeclaredFields()[columnIndex];
        f.setAccessible(true);
        return f.getName();
    }

}

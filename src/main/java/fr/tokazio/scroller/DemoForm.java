package fr.tokazio.scroller;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class DemoForm {

    private final JFrame frame;

    public DemoForm() {
        frame = new JFrame("Scroller");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(800, 300));

        final InfiniteScrollableTable<DemoData> tbl = new InfiniteScrollableTable<>(new DemoDataModel());

        tbl.setLoader(new InfiniteLoader<DemoData>() {

            int nbLoad = 0;

            @Override
            public List<DemoData> load(int from, InfiniteWorker worker) {
                final List<DemoData> l = new LinkedList<>();
                if (nbLoad < 5) {
                    worker.max(32);
                    for (int i = 0; i < 32; i++) {
                        l.add(new DemoData());
                        worker.progress(i);
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    nbLoad++;
                }
                return l;
            }

        });

        frame.add(tbl.asComponent(), BorderLayout.CENTER);
    }

    public DemoForm show() {
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        return this;
    }

}
